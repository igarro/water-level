/**
 * WaterLevelMeter, an pressure based water level meter
 *
 * Copyright (C) 2013 Ignacio Garro
 *
 * WaterLevelMeter is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * WaterLevelMeter is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details. You should have received a copy of the GNU
 * General Public License along with WaterLevelMeter. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/eeprom.h>
#include "sevseg.h"

#define MAXDIFF 30 /* The number of ADC values that the tank full is higher than the tank empty. */

uint16_t min_eeprom EEMEM = 500; /* ADC value for the tank empty. Can be calibrated. */

int measure (void);

int main(void) {
	uint16_t value = 0; /* This is the value from the ADC*/
	uint16_t min;   /*This is the ADC value for the minimum tank measure: empty */
	uint16_t level; /* Last measured tank level.*/
	int loop=0; /* Loop counter. */
	min = eeprom_read_word(&min_eeprom); /* The minimun is stored in eeprom, for calibration.*/

	DDRB = 255;    //PORTB output
	DDRD = 255;    // PORTD output
	DDRC &= ~_BV(PC0); // PC0 (ADC0) input
	DDRC &= ~_BV(PC0); // PC1 (ADC1) input
	sevseg_init(); //initialize seven segments LED

	while (1) {
		/* Measure ADCs*/
		value = measure();
		/* Under min, can happen due to sensor accuracy.*/
		if (value <= min) {
			sevseg_putc('0', 1); /* Signal it with the dot.*/
		}
		/* Over full, can happen.*/
		else if (value >= min+MAXDIFF) {
			sevseg_putc('9', 1); /* Signal it with the dot.*/
		}
		/* ADC value in expected range. */
		else{
			/* Convert ADC value to a level between 0 and 9.*/
			uint16_t new_level=(((value-min)*10)/(MAXDIFF));
			if (new_level!=level){
				level=new_level;
				loop=0;						//Value of level is changing,
											//so user could like to wait without
											//entering calibration.
				sevseg_putc('0'+level, 0);  //Update display
			}
		}
		_delay_ms(500);
		if (loop==10){
			//After 5 secs. enter calibration mode
			sevseg_putc('e', 0); //Warn the user
			_delay_ms(2000); // Last chance to release the button (2 further secs)
			min=value;		// The last value will be used as new min
			eeprom_write_word (&min_eeprom, min); //and write to eeprom
			sevseg_putc('e', 1); //value saved, indicate with the dot.
			_delay_ms(2000);
		}
		loop++;
	}
}


int measure (void){

  	int i=512,j=512;
  	uint16_t diff0=0, diff1=0;
  	uint16_t analog1=0, analog2=0, analog3=0 ;

	while(j){

		while(i){
			ADCSRA=0x80; 		// ADC on, no Prescale
			ADMUX=0;  			// ADC Ref to Avcc, ADC0, format normal
			ADCSRA |=_BV(ADSC); // single conversion mode
			while (ADCSRA & (1<<ADSC)) {;}	// wait for conversion
			diff0=ADCW;
			ADCSRA=0x80; 		// ADC on, no Prescale
			ADMUX=1;  			// ADC Ref to Avcc, ADC1, format normal
			ADCSRA |=_BV(ADSC); // single conversion mode
			while (ADCSRA & (1<<ADSC)) {;}	// wait for conversion
			diff1=ADCW;
			if (diff0>=diff1){
				analog1+=(diff0-diff1);
			}
			else{
				analog1+=(diff1-diff0);
			}
			i--;
		}

		analog2 = analog1/512;
		analog3 += analog2;
		j--;
	}

	analog1=(analog3/512);

	return (analog1);
}
