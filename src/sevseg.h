/*
sevseg lib 0x02

copyright (c) Davide Gironi, 2012

Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/


#ifndef SEVSEG_H_
#define SEVSEG_H_

#include <avr/io.h>

//definitions
#define SEVSEG_TYPECC 0 //common catode type
#define SEVSEG_TYPECA 1 //common anode type

//set type
#define SEVSEG_TYPE SEVSEG_TYPECC

//set display pin, set on same port and ordered
#define SEVSEG_DDR DDRD
#define SEVSEG_PORT PORTD
#define SEVSEG_PINA PD7
#define SEVSEG_PINB PD6
#define SEVSEG_PINC PD5
#define SEVSEG_PIND PD4
#define SEVSEG_PINE PD3
#define SEVSEG_PINF PD2
#define SEVSEG_PING PD1
#define SEVSEG_PINDOT PD0

//set display power port
#define SEVSEG_DDRDISP DDRB
#define SEVSEG_PORTDISP PORTB
#define SEVSEG_PINDISP1 PB0
#define SEVSEG_PINDISP2 PB1

//set display array (of pin)
#define SEVSEG_PINDISPA {SEVSEG_PINDISP1, SEVSEG_PINDISP2}

extern void sevseg_init();
extern void sevseg_putc(uint8_t c, uint8_t dot);
extern void sevseg_puthex(uint8_t h);
extern uint8_t sevseg_selnextdisplay();

#endif
